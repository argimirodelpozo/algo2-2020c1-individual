
#include "string_map.h"

template <typename T>
string_map<T>::string_map() : raiz(nullptr), _size(0) {}


template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.


template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d)
{
    if (raiz) delete raiz;
    raiz = nullptr;

    if (d.raiz) raiz = new Nodo(*d.raiz);

    return *this;
}


template <typename T>
string_map<T>::~string_map()
{
    if (raiz) delete raiz;
    raiz = nullptr;
}


template <typename T>
T& string_map<T>::operator[](const string& clave)
{
    if (!count(clave)) insert(pair<string, T>(clave, T()));
    return at(clave);
}


template <typename T>
int string_map<T>::count(const string& clave) const
{
    if (!raiz || clave.empty()) return 0;

    Nodo* nodoActual = raiz;
    int claveInd = 0;

    while (nodoActual && claveInd < clave.size())
    {
        nodoActual = nodoActual->siguientes[clave[claveInd]];
        claveInd++;
    }

    if (claveInd == clave.size() && nodoActual && nodoActual->definicion) return 1;
    else return 0;
}


template <typename T>
const T& string_map<T>::at(const string& clave) const
{
    Nodo* nodoActual = raiz;
    int claveInd = 0;

    while (claveInd < clave.size())
    {
        nodoActual = nodoActual->siguientes[clave[claveInd]];
        claveInd++;
    }

    const T val = nodoActual->definicion;
    return val;
}


template <typename T>
T& string_map<T>::at(const string& clave)
{
    Nodo* nodoActual = raiz;
    int claveInd = 0;

    while (claveInd < clave.size())
    {
        nodoActual = nodoActual->siguientes[clave[claveInd]];
        claveInd++;
    }

    return *nodoActual->definicion;
}


template <typename T>
void string_map<T>::erase(const string& clave)
{
    stack<Nodo*> nodosVistos;
    Nodo* nodoActual = raiz;

    while (nodosVistos.size() < clave.size())
    {
        int claveInd = nodosVistos.size();
        int letra = int(clave[claveInd]);

        nodosVistos.push(nodoActual);
        nodoActual = nodoActual->siguientes[letra];
    }
    nodoActual->borrarDefinicion();
    _size--;


    while (nodosVistos.size() && !tieneHijos(nodoActual) && !tieneDefinicion(nodoActual))
    {
        int claveInd = nodosVistos.size();

        delete nodoActual;
        nodosVistos.top()->siguientes[clave[claveInd-1]] = nullptr;

        nodoActual = nodosVistos.top();
        nodosVistos.pop();
    }
}


template <typename T>
int string_map<T>::size() const
{
    return _size;
}


template <typename T>
bool string_map<T>::empty() const
{
    return !raiz;
}


template<typename T>
void string_map<T>::insert(const pair<string, T>& val)
{
    if (!raiz) raiz = new Nodo();

    Nodo* nodoActual = raiz;
    int claveInd = 0;
    int tamClave = val.first.length();

    while (claveInd < tamClave)
    {
        int letra = int(val.first[claveInd]);
        Nodo* sigNodo = nodoActual->siguientes[letra];
        if (!sigNodo)
        {
            nodoActual->siguientes[letra] = new Nodo();
            sigNodo = nodoActual->siguientes[letra];
        }
        nodoActual = sigNodo;
        claveInd++;
    }

    if (nodoActual->definicion)
    {
        delete nodoActual->definicion;
        _size--;
    }
    nodoActual->definicion = new T(val.second);
    _size++;
}


template<typename T>
bool string_map<T>::tieneHijos(const Nodo* nodo) const
{
    bool ret = false;
    for (int i = 0; i < 256; i++)
    {
        if (nodo->siguientes[i])
            ret = true;
    }
    return ret;
}