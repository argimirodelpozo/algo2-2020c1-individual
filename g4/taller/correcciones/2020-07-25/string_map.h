#ifndef STRING_MAP_H_
#define STRING_MAP_H_

#include <string>
#include <stack>

#define EXT

using namespace std;

template<typename T>
class string_map {
public:
    /**
    CONSTRUCTOR
    * Construye un diccionario vacio.
    **/
    string_map();

    /**
    CONSTRUCTOR POR COPIA
    * Construye un diccionario por copia.
    **/
    string_map(const string_map<T>& aCopiar);

    /**
    OPERADOR ASIGNACION
     */
    string_map& operator=(const string_map& d);

    /**
    DESTRUCTOR
    **/
    ~string_map();

    /**
    INSERT 
    * Inserta un par clave, valor en el diccionario
    **/
    void insert(const pair<string, T>& val);

    /**
    COUNT
    * Devuelve la cantidad de apariciones de la clave (0 o 1).
    * Sirve para identificar si una clave está definida o no.
    **/

    int count(const string &key) const;

    /**
    AT
    * Dada una clave, devuelve su significado.
    * PRE: La clave está definida.
    --PRODUCE ALIASING--
    -- Versión modificable y no modificable
    **/
    const T& at(const string& key) const;
    T& at(const string& key);

    /**
    ERASE
    * Dada una clave, la borra del diccionario junto a su significado.
    * PRE: La clave está definida.
    --PRODUCE ALIASING--
    **/
    void erase(const string& key);

    /**
     SIZE
     * Devuelve cantidad de claves definidas */
    int size() const;

    /**
     EMPTY
     * devuelve true si no hay ningún elemento en el diccionario */
    bool empty() const;

    /** OPTATIVO
    * operator[]
    * Acceso o definición de pares clave/valor
    **/
    T &operator[](const string &key);

private:

    struct Nodo {
        vector<Nodo*> siguientes;
        T* definicion;

        Nodo() : siguientes(256, nullptr), definicion(nullptr){}
        Nodo(const Nodo& copy) : siguientes(256, nullptr), definicion(nullptr)
        {
            if (copy.definicion) definicion = new T(*copy.definicion);

            for (int i = 0; i < 256; i++)
            {
                if (copy.siguientes[i])
                    siguientes[i] = new Nodo(*copy.siguientes[i]);
                else siguientes[i] = nullptr;
            }
        }
        Nodo(T* def) : siguientes(256, nullptr)
        {
            if (def) definicion = new T(*def);
            else definicion = nullptr;
        }

        ~Nodo()
        {
            borrarDefinicion();
            for (int i = 0; i < 256; i++)
            {
                if (siguientes[i]) delete siguientes[i];
                siguientes[i] = nullptr;
            }
        }

        void borrarDefinicion()
        {
            if (definicion) delete definicion;
            definicion = nullptr;
        }
    };

    Nodo* raiz;
    int _size;

    bool tieneHijos(const Nodo* nodo) const;
    bool tieneDefinicion(const Nodo* nodo) const{ return nodo->definicion; }
};

#include "string_map.hpp"

#endif // STRING_MAP_H_
