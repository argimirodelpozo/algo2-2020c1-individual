
template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr), num(0) {}

template <class T>
Conjunto<T>::~Conjunto() { 
    Destruir(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {

    Nodo* actual = _raiz;
    bool found = false;

    while (!found && actual)
    {
        if (clave > actual->valor) actual = actual->der;
        else if (clave < actual->valor) actual = actual->izq;
        else found = true;
    }

    return found;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (pertenece(clave)) return;

    num++;

    if (!_raiz) _raiz = new Nodo(clave);
    else
    {
        Nodo* actual = _raiz;

        while(actual->valor != clave)
        {
            if (clave > actual->valor)
            {
                if (!actual->der) actual->der = new Nodo(clave);
                actual = actual->der;
            }
            else //clave < actual->valor
            {
                if (!actual->izq) actual->izq = new Nodo(clave);
                actual = actual->izq;
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (!pertenece(clave)) return;

    num--;

    Nodo* ant = _raiz;
    Nodo* actual = _raiz;


    while (actual->valor != clave)
    {
        ant = actual;

        if (clave > actual->valor) actual = actual->der;
        else actual = actual->izq;
    }


    if (ant == actual)
    {
        //es la raiz

        Nodo* AuxIzq = actual->izq;
        Nodo* AuxDer = actual->der;


        if (!AuxIzq && !AuxDer)
        {
            delete actual; _raiz = nullptr;
        }
        else if (AuxIzq && !AuxDer)
        {
            delete actual; _raiz = AuxIzq;
        }
        else if (!AuxIzq && AuxDer)
        {
            delete actual; _raiz = AuxDer;
        }
        else
        {
            T sigClave = siguiente(clave);
            remover(sigClave);
            num++;

            _raiz->valor = sigClave;
        }
        return;
    }


    //3 casos:
    if (!actual->izq && !actual->der)
    {
        if (ant->izq == actual) ant->izq = nullptr;
        else ant->der = nullptr;
        delete actual;
    }
    else if (actual->izq && !actual->der)
    {
        if (ant->izq == actual) ant->izq = actual->izq;
        else ant->der = actual->izq;
        delete actual;
    }
    else if (!actual->izq && actual->der)
    {
        if (ant->izq == actual) ant->izq = actual->der;
        else ant->der = actual->der;
        delete actual;
    }
    else
    {
        T sigClave = siguiente(clave);
        remover(sigClave);
        num++;

        if (ant->izq == actual) ant->izq->valor = sigClave;
        else ant->der->valor = sigClave;
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave)
{
    //Se asume que el elemento existe y que tiene siguiente

    Nodo* actual = _raiz;
    Nodo* ant = _raiz;
    while (actual->valor != clave)
    {
        ant = actual;
        if (clave > actual->valor) actual = actual->der;
        else actual = actual->izq;
    }

    if (!actual->der) return ant->valor;

    //buscar el menor del lado derecho
    actual = actual->der;
    while (actual->izq) actual = actual->izq;


    return actual->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const
{
    //se asume conjunto no vacío

    Nodo* actual = _raiz;
    while (actual->izq) actual = actual->izq;

    return actual->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const
{
    //se asume conjunto no vacío

    Nodo* actual = _raiz;
    while (actual->der) actual = actual->der;

    return actual->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return num;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}


template <class T>
void Conjunto<T>::inOrder(vector<T>& V)
{

}


template <class T>
void Conjunto<T>::Destruir(Nodo* n)
{
    if(n)
    {
        Destruir(n->izq);
        Destruir(n->der);
        delete n; n = nullptr;
    }
}