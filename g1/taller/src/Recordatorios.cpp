/* ENTREGA 1: TALLER DE ALGORITMOS Y ESTRUCTURAS DE DATOS II
 *
 * Nombre: Argimiro del Pozo
 * LU: 540/18
 *
 * */


#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}




// Clase Fecha
class Fecha {
public:
    Fecha(int mes, int dia) : mes_(mes), dia_(dia){}

    int mes(){return mes_;}
    int dia(){return dia_;}

    void incrementar_dia();

    bool operator==(Fecha o);

private:
    int mes_;
    int dia_;
};


void Fecha::incrementar_dia()
{
    if (dia_ < dias_en_mes(mes_)) dia_++;
    else
    {
        mes_ = ((mes_ + 1) % 12);
        dia_ = 1;
    }
}


bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}


ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}




// Clase Recordatorio
class Horario {
public:
    Horario(uint hora, uint min) : hora_(hora), min_(min){}

    uint hora(){return hora_;}
    uint min(){return min_;}

    bool operator==(Horario o);
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;
};


bool Horario::operator==(Horario o)
{
    bool igual_hora = this->hora() == o.hora();
    bool igual_min = this->min() == o.min();
    return igual_hora && igual_min;
}


ostream& operator<<(ostream& os, Horario h)
{
    os << h.hora() << ":" << h.min();
    return os;
}


bool Horario::operator<(Horario h)
{
    if (this->hora() < h.hora()) return true;
    else if (this->hora() == h.hora()) return (this->min() < h.min());
    else return false;
}




// Clase Recordatorio
class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string m) : horario_(h), fecha_(f), mensaje_(m){}

    string mensaje(){return mensaje_;}
    Fecha fecha(){return fecha_;}
    Horario horario(){return horario_;}

private:
    string mensaje_;
    Fecha fecha_;
    Horario horario_;
};


ostream& operator<<(ostream& os, Recordatorio r)
{
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}




// Clase Agenda
class Agenda {
public:
    Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial), recordatorios_() {}
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy(){return hoy_;}

private:
    Fecha hoy_;
    list<Recordatorio> recordatorios_;
};


void Agenda::agregar_recordatorio(Recordatorio rec)
{
    recordatorios_.push_back(rec);
}


void Agenda::incrementar_dia()
{
    for (std::list<Recordatorio>::iterator it=recordatorios_.begin(); it!=recordatorios_.end(); it++)
        if (it->fecha() == hoy_)
            it = recordatorios_.erase(it);

    hoy_.incrementar_dia();
}


list<Recordatorio> Agenda::recordatorios_de_hoy()
{
    list<Recordatorio> TODO_hoy;
    for (std::list<Recordatorio>::iterator it=recordatorios_.begin(); it!=recordatorios_.end(); it++)
    {
        if (it->fecha() == hoy_) TODO_hoy.push_back(*it);
    }

    return TODO_hoy;
}


bool predSort(Recordatorio& first, Recordatorio& second)
{
    first.horario() < second.horario();
}

ostream& operator<<(ostream& os, Agenda a)
{
    list<Recordatorio> recordatoriosDeHoy = a.recordatorios_de_hoy();
    recordatoriosDeHoy.sort(predSort);

    os << a.hoy() << endl;
    os << "=====" << endl;

    for (std::list<Recordatorio>::iterator it = recordatoriosDeHoy.begin(); it != recordatoriosDeHoy.end(); it++)
        os << *it << endl;

    return os;
}