#include "Lista.h"

Lista::Lista() : primero(nullptr), ultimo(nullptr) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {

    while (primero) eliminar(0);
}

Lista& Lista::operator=(const Lista& aCopiar) {

    //borro todos los elementos primero (de haberlos)
    while (primero) eliminar(0);

    Nodo* itCopia = aCopiar.primero;
    while (itCopia)
    {
        agregarAtras(itCopia->data);
        itCopia = itCopia->next;
    }

    return *this;
}

void Lista::agregarAdelante(const int& elem) {

    Nodo* nuevoPrimero = new Nodo(elem);
    nuevoPrimero->next = primero;

    //si ya había por lo menos un elemento, el primero queda en segundo lugar
    //de lo contrario (lista vacía), el ultimo y el primero coinciden, y son este nuevo elemento
    if (primero) primero->prev = nuevoPrimero;
    else ultimo = nuevoPrimero;

    primero = nuevoPrimero;
}

void Lista::agregarAtras(const int& elem) {
    //crear el nuevo nodo
    Nodo* nuevoUltimo = new Nodo(elem);
    nuevoUltimo->prev = ultimo;

    if (ultimo) ultimo->next = nuevoUltimo;
    else primero = nuevoUltimo;

    //el ultimo de la lista será ahora mi nuevoUltimo.
    //Cabe destacar que nuevoUltimo->next = nullptr por el constructor default de Nodo
    ultimo = nuevoUltimo;
}

void Lista::eliminar(Nat i) {

    //Asumo que la longitud de la lista es mayor que i
    //this->longitud() > i

    //Asumo también 0<=i<=this->longitud()-1, donde 0 es eliminar el
    //  elemento apuntado por primero y this->longitud()-1 sería eliminar
    //  el elemento apuntado por ultimo

    Nodo* it = primero;

    while(i)
    {
        it = it->next;
        i--;
    }

    //borrar este elemento
    Nodo* ant = it->prev;
    Nodo* sig = it->next;

    //si no había anterior, era el primer nodo
    if (ant) ant->next = sig;
    else primero = sig;

    //si no había siguiente, era el ultimo nodo
    if (sig) sig->prev = ant;
    else ultimo = ant;

    delete it;
}

int Lista::longitud() const {
    Nat l = 0;

    Nodo* it = primero;
    while (it)
    {
        it = it->next;
        l++;
    }

    return l;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* it = primero;
    while(i)
    {
        it = it->next;
        i--;
    }

    return it->data;
}

int& Lista::iesimo(Nat i) {
    Nodo* it = primero;
    while(i)
    {
        it = it->next;
        i--;
    }

    return it->data;
}

void Lista::mostrar(ostream& o) {
    o << "[";

    Nodo* it = primero;
    while(it)
    {
        o << it->data;
        it = it->next;

        //este if es redundante, pero evita una coma y espacio de mas si it es el último elemento
        if (it) o << ", ";
    }
    o << "]" << endl;
}
